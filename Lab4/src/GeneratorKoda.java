import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Analiza se radi rekurzivno po generativnom stablu koje dobijemo kao input.
 * Ideja:
 * 		- Svaki nezavrsni znak gramatike ima svoju staticku metodu za analizu u klasi Analyzator
 * 		- Svaka ta metoda ima switch-case (ili else if-ove) koji provjerava o kojoj se tocno produkciji tog nezavrsnog znaka radi
 * 		- Dalje se analizira svaka produkcija kako je opisano u poglavlju 4.4.4., 4.4.5. i 4.4.6. uputa
 * 		- Dodatne podatkovne strukture koje ce biti potrebne se dodaju u Analyzator klasu ili cak u ovu
 *		- Ako treba ista mijenjati/dodati u TreeNode klasu recite meni pa cu ja to napraviti
 * @author wex
 *
 */
public class GeneratorKoda {
	
	public static TreeNode genTree;
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		//Ucitavanje generativnog stabla iz stdin
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			List<String> input = new ArrayList<String>();
			
			String line;
			while((line = reader.readLine()) != null && !line.equals("EOF"))
				input.add(line);
			
			genTree = new TreeNode(input);
			reader.close();
		}
			
		//Analiza rekurzija! (logicno)
		
		Analyzator.start(genTree);
		AsGen.finishAssembly();
	}

}

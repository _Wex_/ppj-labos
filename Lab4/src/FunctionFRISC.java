import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author wex
 *
 */
public class FunctionFRISC {
	public List<String> params;
	public List<String> localVars; //USE LABELS!
	public String funcName;
	public String label;
	public StringBuilder code;
	public boolean isVoid;
	
	public FunctionFRISC(List<String> params, String name, boolean isVoid){
		this.isVoid = isVoid;
		this.params = params;
		funcName = name;
		label = "F_" + name;
		code = new StringBuilder();
		localVars = new ArrayList<String>();
	}
	
	/**
	 * TODO: moguce da ce ovu funkciju trebati mijenjati...
	 */
	@Override
	public String toString(){
			return code.insert(0, label + " ").toString();
	}
	
	public void finalizeCode(){
		code.insert(0, AsGen.sub("R7", "%D " + Integer.valueOf(4*localVars.size()).toString(), "R7"));
		code.insert(0, AsGen.move("R7", "R5"));
		code.insert(0, AsGen.push("R5"));
		code.append(funcName + "_END");
		code.append(AsGen.add("R7", "%D " + Integer.valueOf(4*localVars.size()).toString(), "R7"));
		code.append(AsGen.pop("R5"));
		code.append(AsGen.ret());
	}
}

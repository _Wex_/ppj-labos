import java.util.ArrayList;
import java.util.List;


/**
 * U ovu klasu se pohranjuju sve deklaracije/definicije varijabli, 
 * znaci sve DW, DH, DB... 
 * @author wex
 *
 */
public class VarFRISC {
	public String label;
	public String name;
	public String initValue;
	public int arraySize;
	public static int curNrTemp = 0;
	public static int curNrConst = 0;
	public static int curNrArray = 0;
	
	public static VarFRISC createNewConst(){
		VarFRISC con = new VarFRISC();
		con.name = "CONST_" + Integer.valueOf(curNrConst++);
		con.label = con.name;
		con.initValue = "0";
		return con;
	}
	
	public static VarFRISC createNewVar(String name){
		VarFRISC var = new VarFRISC();
		var.name = name;
		var.label = "VAR_" + name;
		var.initValue = "0";
		return var;
	}
	
	public static VarFRISC createNewArray(String name, int size){
		VarFRISC array = new VarFRISC();
		array.arraySize = size;
		array.name = name;
		array.label = "VAR_" + name;
		array.initValue = null;
		StringBuilder sb = new StringBuilder("ARRAY_" + Integer.valueOf(curNrArray++).toString());
		for(int i = 0; i < size; i++)
			sb.append(" DW 0\n");
		Analyzator.globalArrays.append(sb.toString());
		return array;
	}
	
	@Override
	public String toString(){
		return label + " DW " + initValue;
	}
}

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Metode za analizu deklaracija i definicija (poglavlje 4.4.6. u uputama)
 * @author wex
 *
 */
public class Declaration {
	
	public static int initOffset = 0;
	public static String initName = null;
	
	public static int productionEndsWithNIZ_ZNAKOVA(TreeNode node){
		for(int i = 0; i < 15; i++){
			try{
				node = node.getChild(0);
			}
			catch(NullPointerException exc){
				return -1;
			}
		}
		
		if(node.getName().equals("NIZ_ZNAKOVA"))
			return calcLength(node.getLex_atom());
		return -1;
	}
	
	public static int calcLength(String string){
		string = string.substring(1, string.length()-1); //makni navodnike
		int len = string.length();
		char[] a = string.toCharArray();
		
		boolean esc = false;
		for(int i = 0; i < a.length; i++){
			if(esc){
				len--;
				if(!isValidEscapedChar(a[i]))
					return -2;
				esc = false;
			}
			else if(!esc && a[i] == '\\'){
				if(i == a.length-1)
					return -2;
				esc = true;
			}
		}
		return len+1; //dodaj jos nul znak
	}
	
	public static boolean isValidEscapedChar(char c){
		return (c == 't' || c == 'n' || c == '0' || c == '\'' || c == '"' || c == '\\');
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean definicija_funkcije(TreeNode node){

		// 1. produkcija
		if(node.getChild(3).getName().equals("KR_VOID")){
			Analyzator.currentScope = new Scope(Analyzator.currentScope);
			
			boolean isVoid = false;
			
			if(node.getChild(0).getChild(0).getChild(0).getName().equals("KR_VOID"))
				isVoid = true;

			String name = node.getChild(1).getLex_atom();
			Analyzator.curFunction = new FunctionFRISC(new ArrayList<String>(), name, isVoid);
			Analyzator.functionsFRISC.put(name, Analyzator.curFunction);
			Command.slozena_naredba(node.getChild(5));
			Analyzator.curFunction.finalizeCode();
			Analyzator.curFunction = null;
			
			Analyzator.currentScope = Analyzator.currentScope.getParent();
		}

		// 2. produkcija
		else if(node.getChild(3).getName().equals("<lista_parametara>")){
			Analyzator.currentScope = new Scope(Analyzator.currentScope);
			
			boolean isVoid = false;
			if(node.getChild(0).getChild(0).getChild(0).getName().equals("KR_VOID"))
				isVoid = true;
			
			String name = node.getChild(1).getLex_atom();
			FunctionFRISC newFunc = new FunctionFRISC(new ArrayList<String>(), name, isVoid);
			Analyzator.curFunction = newFunc;
			Analyzator.functionsFRISC.put(name, newFunc);
			lista_parametara(node.getChild(3));
			Command.slozena_naredba(node.getChild(5));
			Analyzator.curFunction.finalizeCode();
			Analyzator.curFunction = null;
			
			Analyzator.currentScope = Analyzator.currentScope.getParent();
			
		}

		return true;
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean lista_parametara(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			deklaracija_parametra(node.getChild(0));
		}
		
		// 2. produkcija
		if(node.getChildrenAmount() == 3){
			lista_parametara(node.getChild(0));
			deklaracija_parametra(node.getChild(2));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean deklaracija_parametra(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 2){
			String label = Analyzator.currentScope.addVariable(node.getChild(1).getLex_atom());
			Analyzator.curFunction.params.add(label);
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 4){
			String label = Analyzator.currentScope.addVariable(node.getChild(1).getLex_atom());
			Analyzator.curFunction.params.add(label);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_deklaracija(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			deklaracija(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 2){
			lista_deklaracija(node.getChild(0));
			deklaracija(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean deklaracija(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 3){
			lista_init_deklaratora(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_init_deklaratora(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			init_deklarator(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			lista_init_deklaratora(node.getChild(0));
			init_deklarator(node.getChild(2));
		}
		
		return true;
	}
	
	/**
	 * NOTE: NE SMIJE SA STOGA SKIDATI NASLJEDNO SVOJSTVO!
	 * ALI MORA SKINUTI SA STOGA ONO STO CE STAVITI IZRAVNI DEKLARATOR!!!!
	 * @author wex
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean init_deklarator(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			izravni_deklarator(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			izravni_deklarator(node.getChild(0));
			initName = node.getChild(0).getChild(0).getLex_atom();
			initOffset = 0;
			inicijalizator(node.getChild(2));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izravni_deklarator(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			String name = node.getChild(0).getLex_atom();
			if(Analyzator.currentScope.ID == 0){
				VarFRISC newVar = VarFRISC.createNewVar(name);
				Analyzator.varsFRISC.add(newVar);
				Analyzator.currentScope.addVariable(name);
			}
			else{
				String label = Analyzator.currentScope.addVariable(name);
				Analyzator.curFunction.localVars.add(label);
			}
		}
		
		// 2. produkcija
		else if(node.getChild(2).getName().equals("BROJ")){
			String name = node.getChild(0).getLex_atom();
			if(Analyzator.currentScope.ID == 0){
				VarFRISC newArray = VarFRISC.createNewArray(name, Integer.parseInt(node.getChild(2).getLex_atom()));
				Analyzator.varsFRISC.add(newArray);
				Analyzator.currentScope.addVariable(name);
			}
			else{
				int size = Integer.parseInt(node.getChild(2).getLex_atom());
				String label = Analyzator.currentScope.addArray(name, size);
				Analyzator.curFunction.localVars.add(label);
				for(int i = 0; i < size; i++)
					Analyzator.curFunction.localVars.add(label + "_" + Integer.valueOf(i).toString());
				
				int offset = Analyzator.curFunction.localVars.indexOf(label);
				AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(offset+1)).toString(), "R1"));
				AsGen.addAssembler(AsGen.sub("R5", "R1", "R1"));
				AsGen.addAssembler(AsGen.sub("R1", "4", "R0"));
				AsGen.addAssembler(AsGen.store("R0", "(R1)"));
			}
		}
		
		// 3. produkcija
		else if(node.getChild(2).getName().equals("KR_VOID")){
			//deklaracija funkcije je nebitna za generiranje koda
		}
		
		// 4. produkcija
		else if(node.getChild(2).getName().equals("<lista_parametara>")){
			//deklaracija funkcije je nebitna za generiranje koda
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean inicijalizator(TreeNode node){
		
		// 1.a) produkcija
		if(node.getChildrenAmount() == 1 && productionEndsWithNIZ_ZNAKOVA(node) != -1){
				TreeNode cur = node;
				while(!cur.getName().equals("NIZ_ZNAKOVA"))
					cur = cur.getChild(0);
				String s = cur.getLex_atom().substring(1, cur.getLex_atom().length()-1); //makni navodnike
				List<Character> chars = new ArrayList<Character>();
				for(int i = 0; i < s.length(); i++){
					if(s.charAt(i) == '\\')
						chars.add(Expression.getCharIfEscaped(s.substring(i, (i++)+2)));
					else
						chars.add(s.charAt(i));
				}
				
				String label = Analyzator.currentScope.findLabel(initName);
				if(label != null){
					int offset = Analyzator.curFunction.localVars.indexOf(label);
					for(char c : chars){
						VarFRISC con = VarFRISC.createNewConst();
						con.initValue = "%D " + Integer.valueOf((int) c).toString();
						Analyzator.varsFRISC.add(con);
						AsGen.addAssembler(AsGen.load("R0", "(" + con.label + ")"));
						AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+(initOffset++)+2)).toString() + ")"));
					}
					AsGen.addAssembler(AsGen.move("0", "R0"));
					AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+(initOffset++)+2)).toString() + ")"));
				}
				else{
					AsGen.addAssembler(AsGen.load("R1", "(VAR_" + initName + ")"));
					for(char c : chars){
						VarFRISC con = VarFRISC.createNewConst();
						con.initValue = "%D " + Integer.valueOf((int) c).toString();
						Analyzator.varsFRISC.add(con);
						AsGen.addAssembler(AsGen.load("R0", "(" + con.label + ")"));
						AsGen.addAssembler(AsGen.store("R0", "(R1 + %D " + Integer.valueOf(4*(initOffset++)).toString() + ")"));
					}
					AsGen.addAssembler(AsGen.move("0", "R0"));
					AsGen.addAssembler(AsGen.store("R0", "(R1 + %D " + Integer.valueOf(4*(initOffset++)).toString() + ")"));
				}
		}
		
		// 1.b) produkcija
		else if(node.getChildrenAmount() == 1){
			Expression.izraz_pridruzivanja(node.getChild(0));
			AsGen.addAssembler(AsGen.pop("R0"));
			String label = Analyzator.currentScope.findLabel(initName);
			if(label != null){
				if(Analyzator.curFunction.localVars.contains(label)){
					int offset = Analyzator.curFunction.localVars.indexOf(label);
					AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
				}
			}
			else{
				AsGen.addAssembler(AsGen.store("R0", "(VAR_" + initName + ")"));
			}
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			lista_izraza_pridruzivanja(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_izraza_pridruzivanja(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.izraz_pridruzivanja(node.getChild(0));
			AsGen.addAssembler(AsGen.pop("R0"));
			String label = Analyzator.currentScope.findLabel(initName);
			if(label != null){
				if(Analyzator.curFunction.localVars.contains(label)){
					int offset = Analyzator.curFunction.localVars.indexOf(label);
					AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+(initOffset++)+2)).toString() + ")"));
				}
			}
			else{
				AsGen.addAssembler(AsGen.load("R1", "(VAR_" + initName + ")"));
				AsGen.addAssembler(AsGen.store("R0", "(R1 + %D " + Integer.valueOf(4*(initOffset++)).toString() + ")"));
			}
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			lista_izraza_pridruzivanja(node.getChild(0));
			Expression.izraz_pridruzivanja(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R0"));
			String label = Analyzator.currentScope.findLabel(initName);
			if(label != null){
				if(Analyzator.curFunction.localVars.contains(label)){
					int offset = Analyzator.curFunction.localVars.indexOf(label);
					AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+(initOffset++)+2)).toString() + ")"));
				}
			}
			else{
				AsGen.addAssembler(AsGen.load("R1", "(VAR_" + initName + ")"));
				AsGen.addAssembler(AsGen.store("R0", "(R1 + %D " + Integer.valueOf(4*(initOffset++)).toString() + ")"));
			}
			
		}
		
		return true;
	}
	

}

import java.util.ArrayList;
import java.util.List;

/**
 * Tablica relacije zapocinje znakom kao u knjizi na str. 102. Index sadrzi
 * oznake stupaca i redaka Index su nezavrsni i zavrsni znakovi nadovezani, to
 * su oznake stupca OVA KLASA NUDI SLJEDECE MOGUCNOSTI ---- JE LI OZNACEN PAR
 * ZNAKOVA A I B ---- OZNACI U TABLICI UNOS ZA NIZ A I NIZ B -- NE KORISTI SE
 * OSIM KOD GENERIRANJA TABLICE
 * 
 * @author luka
 *
 */

public class TablicaRelacije {
	public boolean[][] tablica;
	public List<String> index;

	public TablicaRelacije(List<String> nezavrsni, List<String> zavrsni) {
		int velicina = nezavrsni.size() + zavrsni.size();
		tablica = new boolean[velicina][velicina];
		index = new ArrayList<String>();
		index.addAll(nezavrsni);
		index.addAll(zavrsni);
	}

	/**
	 * Je li neki parametar tablice oznacen?
	 * 
	 * @param A
	 * @param B
	 * @return
	 */
	public boolean jeOznacen(String A, String B) {
		if (A.equals("")) {
			return true;
		} else if (B.equals("")) {
			return false;
		}
		int j = index.indexOf(A);
		int i = index.indexOf(B);
		return tablica[i][j];
	}

	/**
	 * Oznaci dani element tablice.
	 * 
	 * @param A
	 * @param B
	 */
	public void oznaci(String A, String B) {

		int i = index.indexOf(A);
		int j = index.indexOf(B);

		tablica[i][j] = true;
	}

	public String toString() {
		int vel = index.size();
		String total = "";
		for (int i = 0; i < vel; i++) {
			String row = index.get(i) + " = <";
			for (int j = 0; j < vel; j++) {
				if (tablica[i][j]) {
					row += index.get(j) + ",";
				}
			}
			total += row + ">|| ";
		}
		return total;
	}

}

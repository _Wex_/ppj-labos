import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Automat ima stanja oznacena LRStavkama.
 * 
 * @author luka
 *
 *         Stanja se nalaze u setu "stanja" a prijelazi u setu "prijelazi"
 */

public class ENKA {
	// ovo su dijelovi automata
	public LinkedHashSet<LRStavka> stanja;
	public Set<Prijelaz> prijelazi = new LinkedHashSet<ENKA.Prijelaz>();
	public LRStavka pocetnoStanje;

	// ovo nisu dijelovi automata nego se koriste za racunanje po algoritmu
	public List<Produkcija> produkcije;
	public TablicaRelacije tablica;
	public Gramatika gramatika;
	public Map<LRStavka, Map<String, List<LRStavka>>> mapaPrijelaza = new LinkedHashMap<LRStavka, Map<String, List<LRStavka>>>();

	public ENKA(Gramatika gramatika, TablicaRelacije tablica) {
		this.gramatika = gramatika;
		this.produkcije = gramatika.produkcije;
		this.tablica = tablica;

		this.stanja = new LinkedHashSet<LRStavka>();

		this.dodajPocetnePrijelaze();

		boolean bilaPromjena = true;
		while (bilaPromjena) {
			Set<LRStavka> novaPronadjenaStanja = new LinkedHashSet<LRStavka>();

			for (LRStavka stanje : stanja) {
				novaPronadjenaStanja.addAll(this.odrediPrijelaze(stanje));
				novaPronadjenaStanja.addAll(this.odrediEpsilonPrijelaze(stanje));
			}

			bilaPromjena = stanja.addAll(novaPronadjenaStanja);
		}

		for (Prijelaz prijelaz : prijelazi) {
			Map<String, List<LRStavka>> mapa = mapaPrijelaza.get(prijelaz.pocetno);
			if (mapa == null) {
				mapa = new LinkedHashMap<String, List<LRStavka>>();
			}
			List<LRStavka> lista = mapa.get(prijelaz.znak);
			if (lista == null) {
				lista = new ArrayList<LRStavka>();
			}
			lista.add(prijelaz.konacno);
			mapa.put(prijelaz.znak, lista);
			mapaPrijelaza.put(prijelaz.pocetno, mapa);
		}
	}

	// 4.a
	public void dodajPocetnePrijelaze() {
		LinkedHashSet<String> skupT = new LinkedHashSet<String>();
		skupT.add("\27");

		pocetnoStanje = new LRStavka(produkcije.get(0), 0, skupT);
		this.stanja.add(pocetnoStanje);

		String pocetna = produkcije.get(0).desnaStrana;
		List<Produkcija> relevantne = new ArrayList<Produkcija>();
		for (Produkcija prod : produkcije) {
			if (prod.lijevaStrana.equals(pocetna)) {
				relevantne.add(prod);
			}
		}

		for (Produkcija prod : relevantne) {
			LRStavka novoStanje = new LRStavka(prod, 0, skupT);
			stanja.add(novoStanje);
			prijelazi.add(new Prijelaz(pocetnoStanje, "", novoStanje));
		}
	}

	// 4.b
	public List<LRStavka> odrediPrijelaze(LRStavka pocetno) {
		if (pocetno.lijevaStrana.equals("q0")) {
			return new ArrayList<LRStavka>();
		}
		// dobavi sva stanja od pocetnog gdje se tocka pomakla udesno i dodaj ih
		List<LRStavka> st = pocetno.stanjaSPomaknutomTockom();
		// nema dalje
		if (st.isEmpty()) {
			return new ArrayList<LRStavka>();
		}
		prijelazi.add(new Prijelaz(pocetno, pocetno.X, st.get(0)));

		for (int i = 0; i < st.size() - 1; i++) {
			LRStavka st1 = st.get(i);
			LRStavka st2 = st.get(i + 1);
			// TODO ovo je mozda greska
			if (st1.X.equals("$")) {
				prijelazi.add(new Prijelaz(st1, "", st2));
			} else {
				prijelazi.add(new Prijelaz(st1, st1.X, st2));
			}

		}

		return st;
	}

	// 4.c
	public List<LRStavka> odrediEpsilonPrijelaze(LRStavka pocetno) {
		Set<String> skupT = new LinkedHashSet<String>();

		if (pocetno.lijevaStrana.equals(this.pocetnoStanje.lijevaStrana)) {
			return new ArrayList<LRStavka>();
		}

		// racunanje skupa T ako je moguce generirati prazni niz onda dodaj sve
		// iz pocetnog
		boolean mozeUPrazan = true;
		List<String> prazni = GSA.prazniNezavrsniZnakovi(produkcije);
		for (int i = pocetno.tockaIndex + 1; i < pocetno.znakoviDesneStrane.length; i++) {
			if (gramatika.zavrsni.contains(pocetno.znakoviDesneStrane[i])) {
				mozeUPrazan = false;
				break;
			} else if (!prazni.contains(pocetno.znakoviDesneStrane[i])) {
				mozeUPrazan = false;
				break;
			}
		}

		if (mozeUPrazan || pocetno.beta.equals("")) {
			skupT.addAll(pocetno.skupDesneStrane);
		}

		// znak b zapocinje niz beta
		for (String b : gramatika.zavrsni) {
			if (GSA.zapocinjeNiz(b, pocetno.beta, tablica)) {
				skupT.add(b);
			}
		}

		List<Produkcija> relevantne = new ArrayList<Produkcija>();
		for (Produkcija prod : produkcije) {
			if (prod.lijevaStrana.equals(pocetno.X)) {
				relevantne.add(prod);
			}
		}

		List<LRStavka> novaStanja = new ArrayList<LRStavka>();
		for (Produkcija prod : relevantne) {
			novaStanja.add(new LRStavka(prod, 0, skupT));
		}

		for (LRStavka stanje : novaStanja) {
			this.prijelazi.add(new Prijelaz(pocetno, "", stanje));
		}

		return novaStanja;
	}

	public class Prijelaz {
		public LRStavka pocetno;
		public String znak;
		public LRStavka konacno;

		public Prijelaz(LRStavka pocetno, String znak, LRStavka konacno) {
			this.pocetno = pocetno;
			this.znak = znak;
			this.konacno = konacno;
		}

		public String toString() {
			return this.pocetno.toString() + "; " + znak + " -> " + this.konacno.toString();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((konacno == null) ? 0 : konacno.hashCode());
			result = prime * result + ((pocetno == null) ? 0 : pocetno.hashCode());
			result = prime * result + ((znak == null) ? 0 : znak.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Prijelaz other = (Prijelaz) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (konacno == null) {
				if (other.konacno != null)
					return false;
			} else if (!konacno.equals(other.konacno))
				return false;
			if (pocetno == null) {
				if (other.pocetno != null)
					return false;
			} else if (!pocetno.equals(other.pocetno))
				return false;
			if (znak == null) {
				if (other.znak != null)
					return false;
			} else if (!znak.equals(other.znak))
				return false;
			return true;
		}

		private ENKA getOuterType() {
			return ENKA.this;
		}

	}
}

import java.util.ArrayList;
import java.util.List;

public class Akcija {
	private String znak; // cak ni ne trebam ovo jer je u hashu
	private String stanje; // to key a meni ne treba za nista unutar klase?
	private String tipAkcije;
	private String sljedeceStanje;
	private Produkcija produkcija;

	private class Produkcija {
		public String lijevaStrana;
		public List<String> desnaStrana;

		public Produkcija(String lijevaStrana) {
			this.lijevaStrana = lijevaStrana;
			desnaStrana = new ArrayList<String>();
		}
	}

	public Akcija(String stanje, String znak, String argument, String tipAkcije) {
		this.znak = znak;
		this.stanje = stanje;
		this.tipAkcije = tipAkcije;
		sljedeceStanje = null;
		produkcija = null;

		if (tipAkcije.equals("reduciraj")) {
			String[] parts = argument.split("->");
			produkcija = new Produkcija(parts[0]);
			if (parts.length < 2)
				return;
			String[] parts2 = parts[1].split(" ");
			for (int i = 0; i < parts2.length; i++)
				produkcija.desnaStrana.add(parts2[i]);
		}

		else if (tipAkcije.equals("pomakni"))
			sljedeceStanje = argument;
	}

	public void izvrsi() {
		if (tipAkcije.equals("pomakni")) {
			SA.stack.push(SA.niz_znakova.get(SA.index++));
			SA.stanja_stack.push(sljedeceStanje);
		} else if (tipAkcije.equals("reduciraj")) {
			Cvor newCvor = new Cvor(produkcija.lijevaStrana);
			List<Cvor> children = new ArrayList<Cvor>();

			if (produkcija.desnaStrana.size() > 0) {
				for (int i = 0; i < produkcija.desnaStrana.size(); i++) {
					children.add((Cvor) SA.stack.pop());
					SA.stanja_stack.pop();
				}

				for (int i = children.size() - 1; i >= 0; i--)
					newCvor.addChild(children.get(i));
			}
			SA.stack.push(newCvor);
			SA.stanja_stack.push(SA.nova_stanja.get((SA.stanja_stack.peek()
					+ " " + produkcija.lijevaStrana).hashCode()));
		}

		else if (tipAkcije.equals("prihvati"))
			SA.prihvacen = true;
	}
}

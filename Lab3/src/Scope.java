import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Klasa za djelokrug
 * NOTE: Koristiti konstruktor koji prima parametar Scope parent!
 * Konstruktor bez parametara se koristi samo jednom i stvara instancu globalnog djelokruga!
 * @author wex
 *
 */
public class Scope {
	
	private Scope parent;
	private Map<String, Var> variables;
	private List<Scope> children;
	
	public Scope(){
		parent = null;
		variables = new HashMap<String, Var>();
		children = new ArrayList<Scope>();
	}
	
	/**
	 * Ovom konstruktoru predaj trenutni aktivni djelokrug, taj trenutni aktivni djelokrug ce biti parent ovom novom.
	 * Predvidena uporaba:
	 * 
	 * currentScope = new Scope(currentScope);
	 * 
	 * Ovo ce stvoriti novi djelokrug kojem ce roditelj biti trenutni djelokrug, 
	 * te jos postavlja trenutni djelokrug na taj novi.
	 * @param parent
	 */
	public Scope(Scope parent){
		this.parent = parent;
		children = new ArrayList<Scope>();
		parent.children.add(this);
		variables = new HashMap<String, Var>();
	}
	
	/**
	 * Ovo vraca djelokrug roditelj ovog djelokruga.
	 * Ovo koristi da se vratis jedan djelokrug iznad.
	 * NOTE: Ne mozes se vratiti nazad u ovaj djelokrug jer ne postoji vise nigdje referenca na njega!
	 * @return
	 */
	public Scope getParent(){
		return parent;
	}
	
	/**
	 * Metoda ce pokusat naci varijablu s navedenim imenom (identifikatorom).
	 * Prvo trazi u svojem djelokrugu pa ide razinu iznad itd...
	 * U prvom djelokrugu kojem nade vratit ce tu varijablu (iz te varijable se mogu saznati tip varijable i jel L_tip)
	 *
	 * Ako nigdje ne pronade deklaraciju ove varijable metoda vraca 'null'.
	 * @param name
	 * @return
	 */
	public Var findVariable(String name){
		if(variables.containsKey(name))
			return variables.get(name);
		
		if(parent != null)
			return parent.findVariable(name);
		
		return null;
	}
	
	/**
	 * Metoda dodaje novu varijablu u ovaj djelokrug
	 * @param name
	 * @param l_type
	 * @param type
	 */
	public void addVariable(String name, boolean l_type, String type){
		variables.put(name, new Var(name, l_type, type));
	}
	
	public Var findFunctionInGlobal(String name){
		if(parent != null)
			return findFunctionInGlobal(name);
		else{
			if(variables.containsKey(name))
				return variables.get(name);
			else
				return null;
		}
	}
	
	public void addFunctionDefToGlobal(String name, List<String> paramTypes, String retType){
		if(parent != null)
			addFunctionDefToGlobal(name, paramTypes, retType);
		else
			variables.put(name, new Var(name, paramTypes, retType, true));
	}
	
	public boolean isDeclaredLocally(String name){
		return variables.containsKey(name);
	}
	
	public void addFunctionDeclaration(String name, List<String> paramTypes, String retType){
		variables.put(name, new Var(name, paramTypes, retType, false));
	}
	
	public boolean checkForMain(){
		if(variables.containsKey("main")){
			Var func = variables.get("main");
			List<String> emptyList = new ArrayList<>();
			if(!(func.checkFunctionTypes(emptyList, "int")))
				return false;
			else
				return true;
		}
		else if(children != null)
			for(Scope scope : children){
				if(scope.checkForMain())
					return true;
			}
		return false;
	}
	
	public boolean checkFunctionDefinitions(){
		HashMap<String, Var> mapOfDefined = new HashMap<String, Var>();
		for(Var var : variables.values())
			if(var.isFunction())
				if(!var.isDefined())
					return false;
				else
					mapOfDefined.put(var.getName(), var);
					
		if(children != null)
			for(Scope child : children)
				if(!child.check(mapOfDefined))
					return false;
		return true;
	}
	
	private boolean check(HashMap<String, Var> map){
		for(Var var : variables.values())
			if(var.isFunction())
				if(!(map.containsKey(var.getName()) && map.get(var.getName()).checkFunctionTypes(var.getParams(), var.getType())))
					return false;
		
		if(children != null)
			for(Scope child : children)
				if(!child.check(map))
					return false;
		return true;
				
	}
	
}

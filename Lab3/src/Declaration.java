import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Metode za analizu deklaracija i definicija (poglavlje 4.4.6. u uputama)
 * @author wex
 *
 */
public class Declaration {
	
	public static int productionEndsWithNIZ_ZNAKOVA(TreeNode node){
		for(int i = 0; i < 15; i++){
			try{
				node = node.getChild(0);
			}
			catch(NullPointerException exc){
				return -1;
			}
		}
		
		if(node.getName().equals("NIZ_ZNAKOVA"))
			return calcLength(node.getLex_atom());
		return -1;
	}
	
	public static int calcLength(String string){
		string = string.substring(1, string.length()-1); //makni navodnike
		int len = string.length();
		char[] a = string.toCharArray();
		
		boolean esc = false;
		for(int i = 0; i < a.length; i++){
			if(esc){
				len--;
				if(!isValidEscapedChar(a[i]))
					return -2;
				esc = false;
			}
			else if(!esc && a[i] == '\\'){
				if(i == a.length-1)
					return -2;
				esc = true;
			}
		}
		return len+1; //dodaj jos nul znak
	}
	
	public static boolean isValidEscapedChar(char c){
		return (c == 't' || c == 'n' || c == '0' || c == '\'' || c == '"' || c == '\\');
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean definicija_funkcije(TreeNode node){

		// 1. produkcija
		if(node.getChild(3).getName().equals("KR_VOID")){

			// 1. provjeri(<ime_tipa>)
			Expression.ime_tipa(node.getChild(0));

			// 2. <ime_tipa>.tip != const(T)
			String type = (String) Analyzator.stack.pop();
			if(type.startsWith("const("))
				Analyzator.printErr(node);

			// 3. provjeri jel ima definirana funkcija IDN.ime
			Var func = Analyzator.currentScope.findFunctionInGlobal(node.getChild(1).getLex_atom());
			if(func != null && func.isDefined())
				Analyzator.printErr(node);

			// 4. ako postoji deklaracija, tipovi se moraju poklapati (void -> <ime_tipa>.tip)
			if(func != null && !func.checkFunctionTypes(new ArrayList<String>(), type))
				Analyzator.printErr(node);

			// 5. zabiljezi definiciju i deklaraciju funkcije
			if(func != null)
				func.setFunctionDefined();
			else
				Analyzator.currentScope.addFunctionDefToGlobal(node.getChild(1).getLex_atom(), new ArrayList<String>(), type);

			// 6. provjeri(<slozena_naredba>)
			Command.slozena_naredba(node.getChild(5));
		}

		// 2. produkcija
		else if(node.getChild(3).getName().equals("<lista_parametara>")){

			// 1. provjeri(<ime_tipa>)
			Expression.ime_tipa(node.getChild(0));

			// 2. <ime_tipa>.tip != const(T)
			String type = (String) Analyzator.stack.pop();
			if(type.startsWith("const("))
				Analyzator.printErr(node);

			// 3. provjeri jel ima definirana funkcija IDN.ime
			Var func = Analyzator.currentScope.findFunctionInGlobal(node.getChild(1).getLex_atom());
			if(func != null && func.isDefined())
				Analyzator.printErr(node);

			// 4. provjeri(<lista_parametara>)
			lista_parametara(node.getChild(3));
			@SuppressWarnings("unchecked")
			List<String> paramTypes = (List<String>) Analyzator.stack.pop();
			@SuppressWarnings("unchecked")
			List<String> paramNames = (List<String>) Analyzator.stack.pop();

			// 5. ako postoji deklaracija, tipovi se moraju poklapati
			if(func != null && !func.checkFunctionTypes(paramTypes, type))
				Analyzator.printErr(node);

			// 6. zabiljezi definiciju i deklaraciju
			if(func != null)
				func.setFunctionDefined();
			else
				Analyzator.currentScope.addFunctionDefToGlobal(node.getChild(1).getLex_atom(), paramTypes, type);

			Analyzator.funcParamsStack.push(paramNames);
			Analyzator.funcParamsStack.push(paramTypes);
			
			// 7. provjeri(<slozena_naredba>) treba napraviti i novi djelokrug s parametrima ove funkcije
			Command.slozena_naredba(node.getChild(5));
		}

		return true;
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean lista_parametara(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			List<String> types = new ArrayList<String>();
			List<String> names = new ArrayList<String>();
			
			// 1. provjeri(<deklaracija_parametra>)
			deklaracija_parametra(node.getChild(0));
			types.add((String) Analyzator.stack.pop());
			names.add((String) Analyzator.stack.pop());
			Analyzator.stack.push(names);
			Analyzator.stack.push(types);
		}
		
		// 2. produkcija
		if(node.getChildrenAmount() == 3){
			List<String> types;
			List<String> names;
			
			// 1. provjeri(<lista_parametara>)
			lista_parametara(node.getChild(0));
			types = (List<String>) Analyzator.stack.pop();
			names = (List<String>) Analyzator.stack.pop();
			
			// 2. provjeri(<deklaracija_parametra>)
			deklaracija_parametra(node.getChild(2));
			String type = (String) Analyzator.stack.pop();
			String name = (String) Analyzator.stack.pop();
			
			// 3. provjeri jel ime vec postoji u parametrima ove funkcije
			for(String existingName : names)
				if(existingName.equals(name))
					Analyzator.printErr(node);
			
			types.add(type);
			names.add(name);
			Analyzator.stack.push(names);
			Analyzator.stack.push(types);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean deklaracija_parametra(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 2){
			
			// 1. provjeri(<ime_tipa>)
			Expression.ime_tipa(node.getChild(0));
			String type = (String) Analyzator.stack.pop();
			String name = node.getChild(1).getLex_atom();
			
			// 2. <ime_tipa>.tip != void
			if(type.equals("void"))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(name);
			Analyzator.stack.push(type);
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 4){
			
			
			// 1. provjeri(<ime_tipa>)
			Expression.ime_tipa(node.getChild(0));
			String type = (String) Analyzator.stack.pop();
			String name = node.getChild(1).getLex_atom();
			
			// 2. <ime_tipa>.tip != void
			if(type.equals("void"))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(name);
			Analyzator.stack.push("niz(" + type + ")");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_deklaracija(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<deklaracija>)
			deklaracija(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 2){
			
			// 1. provjeri(<lista_deklaracija>)
			lista_deklaracija(node.getChild(0));
			
			// 2. provjeri(<deklaracija>)
			deklaracija(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean deklaracija(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<ime_tipa>)
			Expression.ime_tipa(node.getChild(0));
			String type = (String) Analyzator.stack.pop();
			Analyzator.inheritanceStack.push(type);
			
			// 2. provjeri(<lista_init_deklaratora>) uz nasljedno svojstvo
			lista_init_deklaratora(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_init_deklaratora(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<init_deklarator>) uz nasljedno svojstvo
			init_deklarator(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<lista_init_deklaratora>) uz nasljedno svojstvo
			lista_init_deklaratora(node.getChild(0));
			
			// 2. provjeri(<init_deklarator>) uz nasljedno svojstvo
			init_deklarator(node.getChild(2));
		}
		
		return true;
	}
	
	/**
	 * NOTE: NE SMIJE SA STOGA SKIDATI NASLJEDNO SVOJSTVO!
	 * ALI MORA SKINUTI SA STOGA ONO STO CE STAVITI IZRAVNI DEKLARATOR!!!!
	 * @author wex
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean init_deklarator(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<izravni_deklarator>) uz nasljedno svojstvo
			izravni_deklarator(node.getChild(0));
			
			String type = (String) Analyzator.stack.pop();
			
			// 2. <izravni_deklarator>.tip != const(T) && != niz(const(T))
			if(type.startsWith("const(") || type.startsWith("niz(const("))
				Analyzator.printErr(node);
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<izravni_deklarator>) uz nasljedno svojstvo
			izravni_deklarator(node.getChild(0));
			
			String typeDecl = (String) Analyzator.stack.pop();
			int nrElemDecl = -1;
			if(typeDecl.startsWith("niz("))
				nrElemDecl = (int) Analyzator.stack.pop();
			
			// 2. provjeri(<inicijalizator>)
			inicijalizator(node.getChild(2));
			
			Object typeInit = Analyzator.stack.pop(); //TODO weird, na vrhu stoga je u jednom trenu bio boolean? jel to onaj l_type mozda?
			
			// 3. provjeri onu kobasicu iz upute
			
			if(typeDecl.startsWith("niz(")){
				if(!(typeInit instanceof List<?>))
					Analyzator.printErr(node);
				int nrElemInit = (int) Analyzator.stack.pop();
				if(!(nrElemInit <= nrElemDecl))
					Analyzator.printErr(node);
				typeDecl = typeDecl.substring(4, typeDecl.length()-1);
				for(int i = 0; i < nrElemInit; i++)
					if(!(Expression.isImplicitlyCastable(((List<String>) typeInit).get(i), typeDecl)))
						Analyzator.printErr(node);
			}
			
			else{
				if(!(typeInit instanceof String))
					Analyzator.printErr(node);
				if(!(Expression.isImplicitlyCastable((String) typeInit, typeDecl)))
					Analyzator.printErr(node);
			}
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izravni_deklarator(TreeNode node){
		String type = (String) Analyzator.inheritanceStack.peek();
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. ntip != void
			if(type.equals("void"))
				Analyzator.printErr(node);
			
			// 2. IDN.ime nije deklarirano lokalno
			if(Analyzator.currentScope.isDeclaredLocally(node.getChild(0).getLex_atom()))
				Analyzator.printErr(node);
			
			// 3. zabiljezi deklaraciju
			if(!type.startsWith("const("))
				Analyzator.currentScope.addVariable(node.getChild(0).getLex_atom(), true, type);
			else
				Analyzator.currentScope.addVariable(node.getChild(0).getLex_atom(), false, type);
			
			Analyzator.stack.push(type);
		}
		
		// 2. produkcija
		else if(node.getChild(2).getName().equals("BROJ")){
			
			// 1. ntip != void
			if(type.equals("void"))
				Analyzator.printErr(node);
			
			// 2. IDN.ime nije deklarirano lokalno
			if(Analyzator.currentScope.isDeclaredLocally(node.getChild(0).getLex_atom()))
				Analyzator.printErr(node);
			
			// 3. BROJ.vrijednost je izmedu <0, 1024]
			BigInteger value = new BigInteger(node.getChild(2).getLex_atom());
			if(!(value.compareTo(new BigInteger("0")) == 1 && value.compareTo(new BigInteger("1025")) == -1))
				Analyzator.printErr(node);
			
			// 4. zabiljezi deklaraciju
			type = "niz(" + type + ")";
			Analyzator.currentScope.addVariable(node.getChild(0).getLex_atom(), false, type);
			
			Analyzator.stack.push(Integer.valueOf(node.getChild(2).getLex_atom()));
			Analyzator.stack.push(type);
		}
		
		// 3. produkcija
		else if(node.getChild(2).getName().equals("KR_VOID")){
			
			// 1. i 2. ???!?!?!??!
			if(!(Analyzator.currentScope.isDeclaredLocally(node.getChild(0).getLex_atom())))
				Analyzator.currentScope.addFunctionDeclaration(node.getChild(0).getLex_atom(), new ArrayList<String>(), type);
			
			Analyzator.stack.push("funkcija(void->" + type + ")");
		}
		
		// 4. produkcija
		else if(node.getChild(2).getName().equals("<lista_parametara>")){
			
			// 1. provjeri(<lista_parametara>)
			lista_parametara(node.getChild(2));
			
			// 2. i 3.?!?!?!?
			@SuppressWarnings("unchecked")
			List<String> paramTypes = (List<String>) Analyzator.stack.pop();
			if(!(Analyzator.currentScope.isDeclaredLocally(node.getChild(0).getLex_atom())))
				Analyzator.currentScope.addFunctionDeclaration(node.getChild(0).getLex_atom(), paramTypes, type);
			else if(!Analyzator.currentScope.findVariable(node.getChild(0).getLex_atom()).checkFunctionTypes(paramTypes, type))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			Analyzator.stack.push(type);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean inicijalizator(TreeNode node){
		
		// 1.a) produkcija
		int nrElem;
		if(node.getChildrenAmount() == 1 && (nrElem = productionEndsWithNIZ_ZNAKOVA(node)) != -1){
			
			// 1. provjeri(<izraz_pridruzivanja>)
			Expression.izraz_pridruzivanja(node.getChild(0));
			if(nrElem == -2)
				Analyzator.printErr(node);
			
			Expression.izraz_pridruzivanja(node.getChild(0));
			List<String> types = new ArrayList<String>();
			for(int i = 0; i < nrElem; i++)
				types.add("char");
			Analyzator.stack.push(nrElem);
			Analyzator.stack.push(types);
		}
		
		// 1.b) produkcija
		else if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<izraz_pridruzivanja>)
			Expression.izraz_pridruzivanja(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<lista_izraza_pridruzivanja>)
			lista_izraza_pridruzivanja(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_izraza_pridruzivanja(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<izraz_pridruzivanja>)
			Expression.izraz_pridruzivanja(node.getChild(0));
			String type = (String) Analyzator.stack.pop();
			List<String> types = new ArrayList<String>();
			types.add(type);
			Analyzator.stack.push(Integer.valueOf(1));
			Analyzator.stack.push(types);
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<lista_izraza_pridruzivanja>)
			lista_izraza_pridruzivanja(node.getChild(0));
			@SuppressWarnings("unchecked")
			List<String> types = (List<String>) Analyzator.stack.pop();
			int nrElem = (int) Analyzator.stack.pop();
			
			// 2. provjeri(<izraz_pridruzivanja>)
			Expression.izraz_pridruzivanja(node.getChild(2));
			String type = (String) Analyzator.stack.pop();
			types.add(type);
			nrElem++;
			Analyzator.stack.push(nrElem);
			Analyzator.stack.push(types);
		}
		
		return true;
	}
	

}

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa za pojedine cvorove u generativnom stablu. Svaki cvor ima referencu na
 * svojeg parenta i reference na svu svoju djecu.
 * 
 * Reference na djecu ce biti spremljene u listu, te je dijete koje je prije
 * navedeno u listi "lijevije" u produkciji. (BITNO ZA PRODUKCIJE!!)
 * 
 * Reference na djecu ne mogu biti u setu jer je moguce da se pojavi produkcija
 * tipa <Izraz> -> <Izraz> = <Izraz> te se tada oba nezavrsna znaka Izraz ne bi
 * spremila u setu, s listom nema takvih problema.
 * 
 * @author wex
 *
 */
public class TreeNode {
	private String name;
	private TreeNode parent;
	private List<TreeNode> children;
	private static List<String> input;

	// sljedece atribute se koriste jedino u slucaju da je ovaj cvor gen stabla
	// zavrsni znak!
	private String lexic_atom; // leksicka jedinka (koji znakovi koda su
								// grupirani u ovaj zavrsni znak) ako ovo nije
								// zavrsni znak onda je po defaultu ovo null
	private int row; // red u kojem se pojavljuje ova leksicka jedinka (ako je
						// ovo zavrsni znak) ako nije zavrsni znak po defaultu
						// je -1

	/**
	 * Konstruktor izgraduje cijelo generativno stablo na temelju list
	 * stringova, jedan string = jedan red inputa
	 * 
	 * @param input
	 */
	public TreeNode(List<String> input) {
		lexic_atom = null;
		row = -1;

		if (input.size() == 0 || input == null) {
			System.err.println("Nije dobar input za generativno stablo.");
			System.exit(1);
		}

		TreeNode.input = input;

		name = TreeNode.input.get(0);
		parent = null;

		if(!(name.startsWith("<") && name.endsWith(">"))){
			String[] parts = name.split(" ", 3);
			name = parts[0];
			row = Integer.parseInt(parts[1]);
			lexic_atom = parts[2];
			children = null;
		}

		else {
			children = new ArrayList<TreeNode>();

			for (int i = 1; i < input.size(); i++) {
				if (input.get(i).startsWith(" ")
						&& input.get(i).charAt(1) != ' ')
					children.add(new TreeNode(this, i, 1));
			}
		}
	}

	/**
	 * Yup, privatni konstruktor za potrebe public konstruktora...
	 * 
	 * @param parent
	 * @param index
	 * @param depth
	 */
	private TreeNode(TreeNode parent, int index, int depth) {
		this.parent = parent;
		name = input.get(index).substring(depth);
		
		if(!(name.startsWith("<") && name.endsWith(">"))){
			String[] parts = name.split(" ", 3);
			name = parts[0];
			row = Integer.parseInt(parts[1]);
			lexic_atom = parts[2];
			children = null;
		}

		else {
			row = -1;
			children = new ArrayList<TreeNode>();

			String spaces = "";
			for (int i = 0; i <= depth; i++)
				spaces += " ";

			for (int i = index + 1; i < input.size()
					&& input.get(i).startsWith(spaces); i++) {
				if (input.get(i).charAt(depth + 1) != ' ')
					children.add(new TreeNode(this, i, depth + 1));
			}
		}
	}

	/**
	 * Vraca name ovog cvora.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Vraca referencu na TreeNode parent ovog cvora. NOTE: Vratit ce null u
	 * slucaju da se radi o korijenskom cvoru.
	 * 
	 * @return
	 */
	public TreeNode getParent() {
		return parent;
	}

	/**
	 * Metoda vraca broj koji predstavlja koliko djece ima ovaj cvor.
	 * 
	 * @return
	 */
	public int getChildrenAmount() {
		return children.size();
	}

	/**
	 * Vraca referencu na i-to dijete ovog cvora (i = index). Ovo nije nista
	 * drugo nego enkapsulirana metoda get(int index) liste. NOTE: Moze bacit
	 * arrayOutOfBoundsException.
	 * 
	 * @param index
	 * @return
	 */
	public TreeNode getChild(int index) {
		return children.get(index);
	}

	/**
	 * Ako je ovaj cvor list generativnog stabla (tj. nema djecu) onda vraca
	 * "true", inace vraca "false". (DOH!) NOTE: Cvor je list stabla ako i samo
	 * ako je on zavrsni znak!!! ZNACI OVO JE PROVJERA I JE LI OVAJ CVOR ZAVRSNI
	 * ZNAK!!!
	 * 
	 * @return
	 */
	public boolean isTreeLeaf() {
		if (children == null)
			return true;
		return false;
	}

	/**
	 * Vraca leksicku jedinku ovog cvora.
	 * 
	 * @return
	 */
	public String getLex_atom() {
		return lexic_atom;
	}

	@Override
	public String toString() {
		if (row == -1)
			return name;

		else
			return name + "(" + row + "," + lexic_atom + ")";
	}
}
